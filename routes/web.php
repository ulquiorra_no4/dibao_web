<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/',function(){
    return view('sites.index');
});
Route::get('/tin-tuc-n24',function(){
    return view('sites.post');
});
Route::get('/404.html',function(){
    return view('sites.404');
});
Route::get('/ve-dibao-n37',function(){
    return view('sites.about');
});
Route::get('/dich-vu-sau-ban-hang-n42',function(){
    return view('sites.suport');
});
Route::get('/lien-he.html',function(){
    return view('sites.contact');
});
Route::get('daily/{num}/room',function(){
    return view('sites.store_room');
})->where('num','[\d]');
Route::get('daily.html',function(){
    return view('sites.store_page');
});

Route::get('/tin-tuc-dibao.html',function(){
    return view('sites.post_detail');
});
Route::get('/{params}',function(){
    return view('sites.detail');
})->where('params','[\a-z0-9\.-]*\.html');
Route::get('/{category}',function(){
    return view('sites.category');
})->where('category','[\a-z0-9\.-]*');

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-6 i-top">
                <p><span class="fa fa-phone"></span><i>Hotline</i>0903201357</p>
            </div>
            <div class="col-md-3 col-xs-6 i-top">
                <p><span class="fa fa-envelope"></span><i>Email</i>dibao@gmail.com</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="bot-footer">
                <div class="col-md-3 lg-1">
                    <a href="/" class="logo-footer"><img src="pictures/logo_dibaso2x2.png" /></a>
                    <p>Trụ sở chính: Lô D2, Trần Thị Dung, KCN Phúc Khánh, TP Thái Bình</p>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <div class="col-f">
                        <h3>Về chúng tôi</h3>
                        <ul class="list">
                            <li><a href="javascript:;">Giói thiệu về Dibao</a></li>
                            <li><a href="javascript:;">Thư cảm ơn</a></li>
                            <li><a href="javascript:;">Hợp tác kinh doanh</a></li>
                            <li><a href="javascript:;">Tuyển dụng</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-f">
                        <h3>Dịch vụ hậu mãi</h3>
                        <ul class="list">
                            <li><a href="javascript:;">Chính sách bảo hành</a></li>
                            <li><a href="javascript:;">Chính sách bảo mật</a></li>
                            <li><a href="javascript:;">Cam kết chất lượng tốt nhât</a></li>
                            <li><a href="javascript:;">Giao hàng đảm bảo</a></li>
                            <li><a href="javascript:;">Dịch vụ sau bán hàng</a></li>
                            <li><a href="javascript:;">Hình thức thanh toán</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-f">
                        <h3></h3>
                        <ul class="social">
                            <li><a href="javascript:;"><span class="fab fa-facebook-f"></span></a></li>
                            <li><a href="javascript:;"><span class="fab fa-linkedin"></span></a></li>
                            <li><a href="javascript:;"><span class="fab fa-instagram"></span></a></li>
                            <li><a href="javascript:;"><span class="fab fa-youtube"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<section class="module sp">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="inner">
                    <span class="fa fa-globe"></span>
                    <h3>Cửa hàng Dibao ủy nhiệm</h3>
                    <a href="javascript:;" title="Tìm cửa hàng ủy nhiệm gần bạn">Tìm cửa hàng ủy nhiệm gần bạn</a>
                    <i class="fa fa-chevron-right"></i>
                </div>
            </div>
            <div class="col-md-4">
                <div class="inner">
                    <span class="fa fa-expand-arrows-alt"></span>
                    <h3>Dich vụ sau bán hàng</h3>
                    <a href="javascript:;" title="Thông tin về dịch vụ vụ bảo hành và bảo trì">Thông tin về dịch vụ vụ bảo hành và bảo trì</a>
                    <i class="fa fa-chevron-right"></i>
                </div>
            </div>
            <div class="col-md-4">
                <div class="inner">
                    <span class="fa fa-file-alt"></span>
                    <h3>Bảng giá & catalog</h3>
                    <a href="javascript:;" title="Xem bảng giá và catalog mới nhất">Xem bảng giá và catalog mới nhất</a>
                    <i class="fa fa-chevron-right"></i>
                </div>
            </div>
        </div>
    </div>
</section>

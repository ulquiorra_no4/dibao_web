<section class="contact-box">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="inner">
                    <h4>Hotline hỗ trợ</h4>
                    <p>Gọi để được tư vấn về sản phẩm dịch vụ</p>
                    <span class="red">1800 8001</span>
                    <hr>
                    <span class="red">cr@dibao.com.vn</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inner">
                    <h4>Đăng ký nhận tin</h4>
                    <p>Những thông tin về sản phẩm mới, sự kiện và khuyến mãi</p>
                    <div class="form-contact">
                        <form action="" method="post">
                            <div class="input">
                                <input type="text" name="email" value="" placeholder="Nhập email của bạn">
                            </div>
                            <div class="input box">
                                <button class="btn btn-send">Gửi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

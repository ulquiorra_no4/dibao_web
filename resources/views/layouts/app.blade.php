<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome550/css/all.min.css')}}">
    @yield('style')
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
  </head>
  <body>
    @include('layouts.header')
    <main id="main">
        @yield('content')
    </main>
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    @yield('script')
    <a href="javascript:;" id="totop"><span class="fa fa-chevron-up"></span></a>
    @include('layouts.footer')
  </body>
</html>

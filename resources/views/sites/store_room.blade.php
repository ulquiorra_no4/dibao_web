@extends('layouts.app')

@section('content')
    <section class="padding-30t">
        <div class="container">
            <div class="box-head margin-30b">
                <div class="row">
                    <div class="col-md-9">
                        <ul class="brkum1">
                            <li><a href="javascript:;">Miền Bắc</a></li>
                            <li><a href="javascript:;">Hà Nội</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="box">
                            <form action="" method="post">
                                <div class="find-box">
                                    <button><i class="fa fa-search"></i></button>
                                    <input type="text" name="" value="" placeholder="Tìm kiếm">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-t padding-30t">
                <div class="row">
                    <ul class="list box-news3">
                        <li class="col-md-4">
                            <div class="item center">
                                <a href="javascript:;" title="" class="thumb"><img src="pictures/kien-giang.png" alt="" /></a>
                                <a href="javascript:;" title="" class="title">426 Xã Đàn Đống Đa, Hà Nội</a>
                                <p class="addr">Địa chỉ: 126 Xã Đàn, Đống Đa, Hà Nội Hotline: 090 320 1234</p>
                            </div>
                        </li>
                        <li class="col-md-4">
                            <div class="item center">
                                <a href="javascript:;" title="" class="thumb"><img src="pictures/kien-giang.png" alt="" /></a>
                                <a href="javascript:;" title="" class="title">426 Xã Đàn Đống Đa, Hà Nội</a>
                                <p class="addr">Địa chỉ: 126 Xã Đàn, Đống Đa, Hà Nội Hotline: 090 320 1234</p>
                            </div>
                        </li>
                        <li class="col-md-4">
                            <div class="item center">
                                <a href="javascript:;" title="" class="thumb"><img src="pictures/kien-giang.png" alt="" /></a>
                                <a href="javascript:;" title="" class="title">426 Xã Đàn Đống Đa, Hà Nội</a>
                                <p class="addr">Địa chỉ: 126 Xã Đàn, Đống Đa, Hà Nội Hotline: 090 320 1234</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="margin-20b">
        <div class="container">
            <div class="heading">
                <h3>TIN TỨC NỔI BẬT</h3>
            </div>
            <div class="row">
                <ul class="list box-news3">
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Poster-KV-K12L_web-1920x850-01-1@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/img20180520085204036@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/image003_7@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop

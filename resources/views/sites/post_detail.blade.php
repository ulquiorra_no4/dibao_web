@extends('layouts.app')

@section('content')
<section class="module" style="margin-top:20px">
    <div class="container">
        <div class="row">
            <article class="col-md-9 content-left margin-20b">
                <div class="detail-content post-detail">
                    <div class="short">
                        <h1 class="title">Báo điện tử Dân Trí viết về DIBAO</h1>
                        <p class="time"><i class="fa fa-clock"></i> 26/8/2018 06:48</p>
                        <p class="description">Nếu bạn đã quá quen thuộc với nhiều phiên bản xe đạp điện kiểu truyền thống, không lỗi mốt nhưng lại không hề có sự thay đổi khác lạ nào thì ở trên dòng xe máy điện Jeek New Dibao, bạn sẽ hoàn toàn bị thu hút ngay từ cái nhìn đầu tiên bởi phiên bản xe điện</p>
                    </div>
                    <ul class="link-news margin-10b">
                        <li>
                            <a href="javascript:;" title="">Đừng Để Phải Tiếc Nuối Vì Không Lưu Ngay 4 Homestay “Đẹp - Độc - Chất” Ngay Giữa Sài Thành</a>
                        </li>
                        <li>
                            <a href="javascript:;" title="">Đừng Để Phải Tiếc Nuối Vì Không Lưu Ngay 4 Homestay “Đẹp - Độc - Chất” Ngay Giữa Sài Thành</a>
                        </li>
                        <li>
                            <a href="javascript:;" title="">Đừng Để Phải Tiếc Nuối Vì Không Lưu Ngay 4 Homestay “Đẹp - Độc - Chất” Ngay Giữa Sài Thành</a>
                        </li>
                    </ul>
                    <div class="content">

                    </div>
                    <div class="box tag-box">
                        <strong><i class="fa fa-tags"></i> Tags</strong>
                        <ul class="tags-list">
                            @for($i=0;$i<6;$i++)
                            <li><a href="javascript:;" title="">#Huyndai verna</a></li>
                            @endfor
                        </ul>
                    </div>
                </div>
                <div class="" id="comment_box">

                </div>
            </article>
            <aside class="col-md-3 sidebar-right margin-20b">
                <div class="box margin-30b hidden-xs">
                    <form action="" method="post">
                        <div class="find-box">
                            <button><i class="fa fa-search"></i></button>
                            <input type="text" name="" value="" placeholder="tìm kiếm bài viết">
                        </div>
                    </form>
                </div>
                <div class="box">
                    <h3 class="sidebar-title">TIN XEM NHIỀU</h3>
                    <ul class="list sidebar-post">
                        @for($i=0;$i<5;$i++)
                        <li class="row">
                            <a href="javascript:;" class="thumb col-md-4"><img src="/pictures/{{$i%2 ? 'anh-slider-xe-dien-dibao-3.png' : 'xe-dien-vip-rider_00019.png'}}" alt=""></a>
                            <a href="javascript:;" class="title col-md-8">Cần xây dựng trạm sạc cho xe điện tại Việt Nam</a>
                        </li>
                        @endfor
                    </ul>
                </div>
            </aside>
        </div>
    </div>
</section>
<section class="margin-20b">
    <div class="container">
        <div class="heading">
            <h3>Có thể bạn quan tâm</h3>
        </div>
        <div class="row">
            <ul class="list box-news3">
                @for($i=0;$i<2;$i++)
                <li class="col-md-4">
                    <div class="item">
                        <a href="javascript:;" title="" class="thumb"><img src="pictures/Poster-KV-K12L_web-1920x850-01-1@2x.png" alt="" /></a>
                        <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="item">
                        <a href="javascript:;" title="" class="thumb"><img src="pictures/img20180520085204036@2x.png" alt="" /></a>
                        <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="item">
                        <a href="javascript:;" title="" class="thumb"><img src="pictures/image003_7@2x.png" alt="" /></a>
                        <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                    </div>
                </li>
                @endfor
            </ul>
        </div>
    </div>
</section>
@stop

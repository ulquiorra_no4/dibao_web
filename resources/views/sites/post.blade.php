@extends('layouts.app')

@section('content')
<section class="slider margin-20b" style="margin-top:20px">
    <div class="container">
        <div class="carousel" id="main_slider">
            <a href="javascript:;"><img src="pictures/1-01@2x.png" /></a>
            <a href="javascript:;"><img src="pictures/1-01@2x.png" /></a>
        </div>
    </div>
</section>
<section class="module">
    <div class="container">
        <div class="box post-desc margin-20b">
            <h2>Bộ sưu tập ảnh những mẫu xe bán chạy nhất tháng 3 năm 2018</h2>
            <p>Nếu các bạn còn đang suy nghĩ về chọn kiểu dáng xe nào hợp với mình nhất thì có thể xem qua bộ ảnh này và quyết định cuối cùng là chiếc nào nhé. Nếu các bạn còn đang suy nghĩ về chọn kiểu dáng xe nào hợp với mình nhất thì có thể xem qua bộ ảnh này và quyết</p>
        </div>
        <div class="row">
            <article class="col-md-9 content-left margin-20b">
                <ul class="list news-feed">
                    @for($i=0;$i<5;$i++)
                    <li class="row">
                        <a class="col-md-4 thumb" href="javascript:;" title="">
                            <img src="/pictures/{{$i%2 ? 'anh-slider-xe-dien-dibao-3@2x.png' : 'xe-may-dien-butterfly-dibao@2x.png'}}" alt="">
                        </a>
                        <div class="col-md-8 entry">
                            <a href="javascript:;" title="" class="title">Chính thức bắt đầu đăng kí xe máy điện 7/12/2015</a>
                            <div class="margin-10b">
                                <span class="uptime">26/8/2018</span>
                                <a href="javascript:;" title="" class="cate red">Xe điện</a>
                            </div>
                            <p>Trong sáng ngày đầu tiên đăng ký xe máy điện, các phóng viên của Thế giới xe điện đã đi đến các địa điểm từ công an quận đống đa đến hà tây, hà nam, nam đinh, thái bình đều diễn ra cực kỳ xuôn sẻ và thuận lợi  Hoàn toàn miễn phí 100% </p>
                        </div>
                    </li>
                    @endfor
                </ul>
                <div class="box center">
                    <ul class="paging">
                        <li><a href="javascript:;"><i class="fa fa-chevron-left"></i></a></li>
                        <li class="active"><a href="javascript:;">1</a></li>
                        <li><a href="javascript:;">2</a></li>
                        <li><a href="javascript:;">3</a></li>
                        <li><a href="javascript:;"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                </div>
            </article>
            <aside class="col-md-3 sidebar-right margin-20b">
                <div class="box margin-30b hidden-xs">
                    <form action="" method="post">
                        <div class="find-box">
                            <button><i class="fa fa-search"></i></button>
                            <input type="text" name="" value="" placeholder="tìm kiếm bài viết">
                        </div>
                    </form>
                </div>
                <div class="box">
                    <h3 class="sidebar-title">TIN XEM NHIỀU</h3>
                    <ul class="list sidebar-post">
                        @for($i=0;$i<5;$i++)
                        <li class="row">
                            <a href="javascript:;" class="thumb col-md-4"><img src="/pictures/{{$i%2 ? 'anh-slider-xe-dien-dibao-3.png' : 'xe-dien-vip-rider_00019.png'}}" alt=""></a>
                            <a href="javascript:;" class="title col-md-8">Cần xây dựng trạm sạc cho xe điện tại Việt Nam</a>
                        </li>
                        @endfor
                    </ul>
                </div>
            </aside>
        </div>
    </div>
</section>
<section class="margin-20b">
    <div class="container">
        <div class="heading">
            <h3>TIN TỨC NỔI BẬT</h3>
        </div>
        <div class="row">
            <ul class="list box-news3">
                <li class="col-md-4">
                    <div class="item">
                        <a href="javascript:;" title="" class="thumb"><img src="pictures/Poster-KV-K12L_web-1920x850-01-1@2x.png" alt="" /></a>
                        <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="item">
                        <a href="javascript:;" title="" class="thumb"><img src="pictures/img20180520085204036@2x.png" alt="" /></a>
                        <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="item">
                        <a href="javascript:;" title="" class="thumb"><img src="pictures/image003_7@2x.png" alt="" /></a>
                        <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>
@include('layouts.inc_sp')
@include('layouts.inc_cont')
@stop
@section('style')
<link href="{{ asset('plugins/nivo-slider/nivo-slider.css') }}" rel="stylesheet">
<link href="{{ asset('nivo-slider/themes/default/default.css') }}" rel="stylesheet">
@stop
@section('script')
<script type="text/javascript" src="{{asset('plugins/nivo-slider/jquery.nivo.slider.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/nivo-slider/jquery.nivo.slider.pack.js')}}"></script>
<script type="text/javascript">
    $('#main_slider').nivoSlider({
        animSpeed: 1000,
        manualAdvance:true,
        controlNav:false
    });
</script>
@stop

@extends('layouts.app')

@section('content')
    <section class="slider">
        <div class="carousel" id="main_slider">
            <a href="javascript:;"><img src="pictures/PC-1920x850-2 copy@2x.png" /></a>
            <a href="javascript:;"><img src="pictures/PC-1920x850-2 copy@2x.png" /></a>
        </div>
    </section>
    <section class="mid-box">
        <div class="container">
            <ul class="row list">
                <li class="col-md-4">
                    <a href="/ve-dibao-n37">
                        <img src="pictures/Poster-KV-K12L_web-1920x850-01-1.png" />
                        <div class="entry">
                            <span>Về chúng tôi</span>
                            <span class="fa fa-chevron-circle-right"></span>
                        </div>
                    </a>
                </li>
                <li class="col-md-4">
                    <a href="/san-pham">
                        <img src="pictures/img20180520085204036.png" />
                        <div class="entry">
                            <span>Sản phẩm</span>
                            <span class="fa fa-chevron-circle-right"></span>
                        </div>
                    </a>
                </li>
                <li class="col-md-4">
                    <a href="/tin-tuc-n24">
                        <img src="pictures/air-blade-2017-mau-den-mo-compressor.png" />
                        <div class="entry">
                            <span>Tin tức</span>
                            <span class="fa fa-chevron-circle-right"></span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <div class="container">
        <div class="adv">
            <a href="javascript:;"><img src="pictures/PC-CUB-1920x850_FA-01-1@2x.png" /></a>
        </div>
    </div>
    <section class="module">
        <div class="container">
            <div class="heading">
                <a href="/noi-bat"><h3>SẢN PHẨM NỔI BẬT</h3></a>
            </div>
            <div class="row">
                <ul class="list list-product">
                    @for($i=0;$i<6;$i++)
                    <li class="col-md-4">
                        <div class="inner">
                            <div class="thumb">
                                <a href="/xe-dien-vespas-dibao.html" title=""><img src="pictures/xe-dien-vespas-dibao@2x.png" alt="" /></a>
                                <div class="shadow-info">
                                    <strong>Đặc điểm nổi bật</strong>
                                    <p>phanh đĩa trước</p>
                                    <p>Xe điện Dibao Nami mang một thiết kế hoàn toàn mới nhỏ gọn, thanh thoát điều này thể hiện rõ nét qua mặt trước của xe</p>
                                    <p>Đồng hồ điện tử</p>
                                </div>
                            </div>
                            <div class="entry">
                                <a href="/xe-dien-vespas-dibao.html" title="Xe điện Dibao" class="title">Xe điện Dibao</a>
                                <div class="pr">
                                    Giá bán từ : <span class="price">15.000.000đ</span>
                                </div>
                                <a href="/xe-dien-vespas-dibao.html" title="Chi tiết" class="link-detail">Xem chi tiết <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                    </li>
                    @endfor
                </ul>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    <section class="module news">
        <div class="container">
            <div class="heading">
                <h3>TIN TỨC NỔI BẬT</h3>
            </div>
            <div class="row">
                <ul class="list box-news3">
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Poster-KV-K12L_web-1920x850-01-1@2x.png" alt="" /></a>
                            <span class="uptime">01 tháng 3 năm 2018</span>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/img20180520085204036@2x.png" alt="" /></a>
                            <span class="uptime">01 tháng 3 năm 2018</span>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/image003_7@2x.png" alt="" /></a>
                            <span class="uptime">01 tháng 3 năm 2018</span>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row hidden-xs">
                <div class="col-md-6">
                    <div class="inner">
                        <a href="javascript:;" title=""><img src="pictures/3174@2x.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <ul class="list news-right">
                        <li class="item row">
                            <div class="col-xs-6">
                                <a href="javascript:;" title=""><img src="pictures/PC-CUB-1920x850_FA-01.png" alt="" /></a>
                            </div>
                            <div class="col-xs-6">
                                <div class="info-right">
                                    <a href="javascript:;" title="" class="title">Mercedes Benz Leaders learn Vital Bike Shop Lesson</a>
                                    <p>Xe đạp điện là một phương tiện giao thông mới, hiện nay xe có rất nhiều kiểu dáng phong phú, chất lượng khác nhau.</p>
                                </div>
                            </div>
                        </li>
                        <li class="item row">
                            <div class="col-xs-6">
                                <a href="javascript:;" title="" ><img src="pictures/Sur_Ron-8__44179.1513042993.png" alt="" /></a>
                            </div>
                            <div class="col-xs-6">
                                <div class="info-right">
                                    <a href="javascript:;" title="" class="title">Mercedes Benz Leaders learn Vital Bike Shop Lesson</a>
                                    <p>Xe đạp điện là một phương tiện giao thông mới, hiện nay xe có rất nhiều kiểu dáng phong phú, chất lượng khác nhau.</p>
                                </div>
                            </div>
                        </li>
                        <li class="item row">
                            <div class="col-xs-6">
                                <a href="javascript:;" title=""><img src="pictures/PC-CUB-1920x850_FA-01.png" alt="" /></a>
                            </div>
                            <div class="col-xs-6">
                                <div class="info-right">
                                    <a href="javascript:;" title="" class="title">Mercedes Benz Leaders learn Vital Bike Shop Lesson</a>
                                    <p>Xe đạp điện là một phương tiện giao thông mới, hiện nay xe có rất nhiều kiểu dáng phong phú, chất lượng khác nhau.</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="center" style="margin-top:15px;">
                <a href="javascript:;" title="Xem Thêm" class="link-detail">Xem Thêm <span class="fa fa-chevron-right"></span></a>
            </div>
        </div>
    </section>
    @include('layouts.inc_cont')
    <div class="" id="tool_neo">
        <ul>
            <li><a href="javascript:;"><span class="fab fa-facebook-messenger"></span></a></li>
            <li><a href="javascript:;" onclick="showModal(2)"><span class="fa fa-phone"></span></a></li>
            <li><a href="javascript:;"><span class="fa fa-map-marker-alt"></span></a></li>
        </ul>
    </div>
    @include('layouts.modal_hotline')
@stop
@section('style')
<link href="{{ asset('plugins/nivo-slider/nivo-slider.css') }}" rel="stylesheet">
<link href="{{ asset('nivo-slider/themes/default/default.css') }}" rel="stylesheet">
@stop
@section('script')
<script type="text/javascript" src="{{asset('plugins/nivo-slider/jquery.nivo.slider.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/nivo-slider/jquery.nivo.slider.pack.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $('#main_slider').nivoSlider({
        animSpeed: 1000,
        manualAdvance:true,
        controlNav:false
    });
</script>
@stop

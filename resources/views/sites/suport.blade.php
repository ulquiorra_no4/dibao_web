@extends('layouts.app')

@section('content')
    <section class="padding-40t">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="detail-content">
                        <h2>Dịch vụ sau bán hàng</h2>
                        <strong>Những điều cần lưu ý</strong>
                        <p>Là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                        <p>Là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                        <p>Là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                        <p>Là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                        <p>Là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                        <p>Là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                        <p>Là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="margin-20b">
        <div class="container">
            <div class="heading">
                <h3>TIN TỨC NỔI BẬT</h3>
            </div>
            <div class="row">
                <ul class="list box-news3">
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Poster-KV-K12L_web-1920x850-01-1@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/img20180520085204036@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/image003_7@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop

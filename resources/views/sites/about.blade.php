@extends('layouts.app')

@section('content')
    <section class="module padding-40t">
        <div class="detail-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 about-detail">
                        <h2>Giới thiệu về Dibao</h2>
                        <p>Công ty TNHH TM Tấn Thành tiền thân là Trung tâm Lắp ráp và sản xuất điện cơ BENLIN, đi vào hoạt động từ năm 2001, với hơn 14 năm kinh nghiệm trong lĩnh vực nhập khẩu và phân phối xe điện đến nay chúng tôi đã trở thành một đơn vị nhập khẩu và phân phối xe đạp điện uy tín hàng đầu Việt Nam.</p>
                        <p class="center"><img src="pictures/about.png" /></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="vision-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>PHƯƠNG CHÂM HOẠT ĐỘNG</h2>
                        <ul class="list row">
                            @for($i=0;$i<6;$i++)
                            <li class="col-md-4">
                                <div class="inner">
                                    <strong>Khách hàng là trên hết</strong>
                                    <p>Tất cả quyết định của chúng tôi xoay quanh việc tạo thêm giá trị cho khách hàng.</p>
                                </div>
                            </li>
                            @endfor
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="center">CÁC DÒNG XE NỔI BẬT</h3>
                    <ul class="list flex-box top-manuf">
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Image 3@2x.png" /><span>Bridgestone</span></a>
                        </li>
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Rectangle 466@2x.png" /><span>Honda</span></a>
                        </li>
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Rectangle 467@2x.png" /><span>Yamaha</span></a>
                        </li>
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Rectangle 468@2x.png" /><span>NIJIA (MAXBIKE)</span></a>
                        </li>
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Rectangle 469@2x.png" /><span>Sunra</span></a>
                        </li>
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Rectangle 470@2x.png" /><span>Dibao</span></a>
                        </li>
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Rectangle 471@2x.png" /><span>Giant</span></a>
                        </li>
                        <li>
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Rectangle 472@2x.png" /><span>Yadea</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="margin-20b">
        <div class="container">
            <div class="heading">
                <h3>TIN TỨC NỔI BẬT</h3>
            </div>
            <div class="row">
                <ul class="list box-news3">
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/Poster-KV-K12L_web-1920x850-01-1@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/img20180520085204036@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item">
                            <a href="javascript:;" title="" class="thumb"><img src="pictures/image003_7@2x.png" alt="" /></a>
                            <a href="javascript:;" title="" class="title">Bike News Roundup: The future of Mobilily</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop
